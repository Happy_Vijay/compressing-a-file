#define QMAX 5000
typedef struct triplet{
	unsigned short pos;
	unsigned short length;
	unsigned char ch;
}triplet;
typedef struct queue{
	triplet array[QMAX];
	int i, j, count;
}queue;
void init(queue *q);
void enqueue(queue *q, triplet t);
triplet dequeue(queue *q);
int isqEmpty(queue *q);
int isqFull(queue *q);
