#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include<string.h>
#include"circularq.h"
#include<stdlib.h>
#include<stdio.h>
#define MAX 4000
#define MAXHASH 20000
typedef struct node{
	char *str;
	int pos;
	struct node *next;
}node;
unsigned char dictionary[MAX];
node *hashTable[MAXHASH];
void initHash();
int search(int source, int dest);
void store(int source, int dest);
/* We need to store triplets as <pos, len, char>
*/
int state = 0;
int main(int argc, char *argv[]){
	int dictpos = 0, l = 0, i, d;
	int max;
	int pos = -1, j, tpos = -1;
	int fr, fw;
	unsigned char ch;
	queue q;
	int half;
	triplet t;
	init(&q);
	if(argc != 2){
		printf("Enter arguments to main as <G./zip> <Compress/Decompress> <File1> <File2>\n");
		exit(1);
	}
	fr = open(argv[1], O_RDONLY);
	if(fr == -1){
		printf("Open Failed\n");
		exit(1);
	}
	while(1){
		l = 0;
		dictpos = 0;
		initHash();
		while(l < MAX && read(fr, &ch, 1)){
			dictionary[l++] = ch;
		}
		max = l;
		store(dictpos, 1);
		t.pos = t.length = 0;
		t.ch = dictionary[dictpos];
		enqueue(&q, t);
		dictpos++;
		half = l / 2;
		while(dictpos < max){
			i = 1;
			d = dictpos;
			pos = -1;
			while(dictpos + i < max && (tpos = search(dictpos, i)) != -1){
				pos = tpos;
				i = i + 1;
			}
			//printf("%d\n", pos);
			t.length = i - 1;
			if(pos == -1){
				t.pos = 0;
			}
			else{
				t.pos = dictpos - pos;
			}
			t.ch = dictionary[dictpos + i - 1];
			//printf("%d ", state++);
			if(dictpos == max){
				break;	
			}
			enqueue(&q, t);
			dictpos = d + i;
			for(j = 0; j <= dictpos; j++){
				if(dictpos - j <= half){
					store(j, dictpos - j);
				}
			}
		}
		while(!isqEmpty(&q)){
		t = dequeue(&q);
		printf("<%d:%d:%c>\n", t.pos, t.length, t.ch);
		}
		if(l < MAX){
			break;
		}
	}
	return 0;
}
void initHash(){
	int i = 0;
	for(i = 0; i < MAXHASH; i++){
		hashTable[i] = NULL;
	}
}
int hash(int source, int dest){
	int sum = 0;
	int i;
	int sign = 1;
	for(i = 0; i < dest; i++){
		sum = sum + sign * dictionary[source + i];
		sign = sign * -1;
	}
	if(sum < 0){
		sum = sum * -1;
	}
	return sum;
}
/* Chaining */
void store(int source, int dest){
	int i, k = 0;
	int sum;
	node *temp = malloc(sizeof(node));
	if(!temp){
		return;
	}
	temp -> str = malloc(dest + 1);
	temp -> pos = source;
	for(i = 0; i < dest; i++){
		temp -> str[k++] = dictionary[source + i];
	}
	temp -> str[k] = '\0';
	temp -> next = NULL;
	sum = hash(source, dest);
	//printf("%d\n", sum);
	if(hashTable[sum] == NULL){
		hashTable[sum] = temp;
	}
	else{
		temp -> next = hashTable[sum];
		hashTable[sum] = temp;
	}
}
int search(int source, int dest){
	int sum = hash(source, dest);
	int k = 0, i;
	unsigned char str[MAX];
	int rval;
	node *p;
	char *s;
	if(hashTable[sum] == NULL){
		return -1;
	}
	p = hashTable[sum];
	while(p){
		if(!strcmp(str, p -> str)){
			rval = p -> pos;
			p -> pos = source;
			return rval;
		}
		p = p -> next;
	}
	return -1;
}
