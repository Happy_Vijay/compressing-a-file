#include"circularq.h"
void init(queue *q){
	q -> i = q -> j = q -> count = 0;
}
void enqueue(queue *q, triplet t){
	//Store at i, increment i
	//Check if we have reached at end
	q -> array[q -> i] = t;
	q -> i = (q -> i + 1) % QMAX;
	q -> count++;
}
triplet dequeue(queue *q){
	triplet t;
	//CVheck if we have reached the end
	//Remove value at j, then decrement j
	t = q -> array[q -> j];
	q -> j = (q -> j + 1) % QMAX;
	q -> count--;
	return t;
}
int isqEmpty(queue *q){
	return(q -> count == 0);
}
int isqFull(queue *q){
	return (q -> count == QMAX);
}
