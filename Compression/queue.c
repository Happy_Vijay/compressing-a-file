#include"queue.h"
#include<string.h>
#include<stdlib.h>
void qinit(queue *q){
	q -> count = q -> rear = q -> front = 0;
}
void enqueue(queue *q, unsigned int m){
	q -> array[q -> rear] = m;
	q -> rear = (q -> rear + 1) % IMAX;
	q -> count++;
}
unsigned int dequeue(queue *q){
	unsigned int m;
	m = q -> array[q -> front];
	q -> front = (q -> front + 1) % IMAX;
	q -> count--;
	return m;
}
int isqEmpty(queue *q){
	return (q -> count == 0);
}
int isqFull(queue *q){
	return (q -> count == IMAX);
}
