#include"htree.h"
#include<stdio.h>
#include<stdlib.h>
#define SWAP(x, y, t) ((t) = (x), (x) = (y), (y) = (t))
/*Initializes the tree*/
void init(htree *h){
	h -> n = 0;
}
/* Creates a new node */
node* createNode(unsigned long long freq, char ch){
	node *m;
	m = (node *)malloc(sizeof(node));
	if(m == NULL){
		return m;
	}
	m -> freq = freq;
	m -> ch = ch;
	m -> left = m -> right = NULL;
	return m;
}
/* Inserts a New Node in the Min-Heap */
void insertNode(htree *h, node *m){
	int child = h -> n;		// Child node
	int par = (child - 1) / 2;	// Parent Node
	node *temp;		//Temporary Node for Swapping
	h -> array[h -> n] = m;		//Assign the node to last index
	/* Now do adjustments */
	while(child > 0){
		if(((h -> array[child])-> freq) < ((h -> array[par]) -> freq)){
			SWAP(h -> array[child], h -> array[par], temp);
		}
		else{
			break;		//Element is inserted at right position
		}
		child = (child - 1) / 2;
		par = (par - 1) / 2;
	}
	h -> n++;		// Increment the current position
}
/*Removes the node from tree and return it's address*/
node* removeNode(htree *h){
	node *m = h ->array[0];		// Since it's a min-heap
	node *temp;			// Temporary variable for Swapping
	int p = 0;			// Parent Node
	int c1 = 2 * p + 1;		// Left Child
	int c2 = 2 * p + 2;		// Right Child
	int s;				// To store the index of array with smallest freq-node
	h -> array[0] = h -> array[h -> n - 1];
	h -> n--;
	/*Now do adjustments*/
	while(c1 <= h -> n && c2 <= h -> n){
		/* Find the smallest node */
		if((h -> array[p] -> freq) < (h -> array[c1] -> freq) && (h -> array[p] -> freq) < (h -> array[c2] -> freq)){
			s = p;
		}
		else if((h -> array[c1] -> freq < h -> array[p] -> freq) && (h -> array[c1] -> freq < h -> array[c2] -> freq)){
			s = c1;
		}
		else{
			s = c2;
		}
		if(s == c1){
			SWAP(h -> array[p], h -> array[c1], temp);//if c1 is smaller than parent, SWAP it with parent
			p = c1;
		}
		else if(s == c2){
			SWAP(h -> array[p], h -> array[c2], temp);//if c2 is smaller than parent, SWAP it with parent
			p = c2;
		}
		else{
			break;			//if parent is smallest, we have found the right position
		}
		c1 = 2 * p + 1;
		c2 = 2 * p + 2;
	}
	return m;
}
void createAndInsertNode(htree *h, unsigned long long freq, char ch){
	node *m;
	m = (node *)malloc(sizeof(node));
	if(m == NULL){
		return;
	}
	m -> freq = freq;
	m -> ch = ch;
	m -> left = m -> right = NULL;
	insertNode(h, m);
}
int isTreeEmpty(htree *h){
	return (h -> n == 0);
}
int isTreeFull(htree *h){
	return (h -> n == MAX);
}
