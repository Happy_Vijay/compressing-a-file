#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<stdlib.h>
#include<string.h>
#include"htree.h"
#include<math.h>
#include"queue.h"	//For Integer Queue
#include"cqueue.h"	//For Character Queue
/* Global Variables Declarations */

unsigned int size;		//Stores Number of distinct characters in File
extern struct huff array[128];	//Global array that will contain huffman's code for all characters.
int fr;				// File descriptor of orignal file
int fw;				// File descriptor for compressed file

/* Function Prototypes */
void huffman(unsigned long long *ascii);	//This Function builds a huffman tree, assigns Huffman code for each character
void writeHeader();		// To write a header that desribes huffmann codes
short convertStringtoShort(char *str); 
void writeData();		// Writes data in compressed Fashion
void getHuffmanCode(char ch, char *str);

int main(int argc, char *argv[]){
	char ch;
	int i;
	unsigned long long ascii[128] = {0};
	if(argc != 3){
		printf("User to enter two files, first to be compressed and second that is compressed version of first\n");
		exit(1);
	}
	fr = open(argv[1], O_RDONLY);
	if(fr == -1){
		printf("Open Failed");
		exit(1);
	}
	while(read(fr, &ch, 1)){
		ascii[ch - '\0']++;
	}
	fw = open(argv[2], O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
	if(fw == -1){
		printf("Open Failed\n");
		exit(1);
	}
	/* This Function builds a huffman tree, assigns Huffman code for each character and stores them in array */
	huffman(ascii);
	/* Determine number of distinct characters */	
	for(i = 0; i < 128; i++){
		if(strlen(array[i].str)){
			size++;
		}
	}
	lseek(fr, SEEK_SET, 0);
	writeHeader();
	writeData();
	return 0;
}
/* My header is of form
 <char ch>     |	   <char len>            |                <short sh>
 for character |	 length of huffmann code|		Huffmann Code
*/
void writeHeader(){
	int i;
	char ch, len;
	short sh;
	write(fw, &size, sizeof(int));
	for(i = 0; i < 128; i++){
		if(strlen(array[i].str)){		// Write only those characters that exits in the File
			ch = array[i].ch;
			write(fw, &ch, 1);
			len = strlen(array[i].str);
			write(fw, &len, 1);
			sh = convertStringtoShort(array[i].str);
			write(fw, &sh, sizeof(short));
		}	
	}
}
short convertStringtoShort(char *str){
	short sh = 0;
	int i, l = strlen(str);
	for(i = 0; i < l; i++){
		if(str[i] == '1'){
			sh = sh + pow(2, l - 1 - i);
		}
	}
	return sh;
}
/* This Function writes characters in file by using variable Huffman's Code */
/* The variable codes are cascaded into a single unsigned integers using binary conversions and queues */
void writeData(){
	char ch;
	char str[64];
	unsigned int m;
	unsigned long long datasize = 0;
	cqueue cq;
	queue q;
	cqinit(&cq);
	qinit(&q);
	while(read(fr, &ch, 1)){
		getHuffmanCode(ch, str);
		enqueueFromString(&cq, str);
		if(noOfCharacters(&cq) >= 32){
			m = getIntegerFromQueue(&cq);
			enqueue(&q, m);
			datasize++;
		}
	}
	m = noOfCharacters(&cq);
	enqueue(&q, m);
	m = getIntFromQueue(&cq);
	enqueue(&q, m);
	datasize += 2;
	write(fw, &datasize, sizeof(long long));
	printf("%lld", datasize);
	while(!isqEmpty(&q)){
		m = dequeue(&q);
		write(fw, &m, sizeof(int));
	}
}
void getHuffmanCode(char ch, char *str){
	strcpy(str, array[ch].str);
}
