#include"htree.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
struct huff array[128];		//Global array that will contain huffman's code for all characters.
void leafOrder(node *h);	// Function to visit each leaf node recursively
void initializearray(struct huff *array);
void huffman(unsigned long long *ascii){
	int i;
	unsigned long long f;
	htree h;
	node *p, *q, *r;
	init(&h);
	/* Create a node for each character present in file and insert it into min-heap */
	for(i = 0; i < 128; i++){
		if(ascii[i]){
			createAndInsertNode(&h, ascii[i], i + '\0'); //Take only non- zero frequency characters
		}	
	}
	/* Now that we have created a min-heap, we are ready to build the huffman tree */
	while(1){
		/* Remove two nodes from min-heap */
		if(!isTreeEmpty(&h)){
			p = removeNode(&h);
		}
		else{
			break;
		}
		if(!isTreeEmpty(&h)){
			q = removeNode(&h);
		}
		/*If min heap has only one node left, we are done*/
		else{
			break;
		}
		/* Create a node with freq. equal to sum of the two min - nodes and add it again two min heap */
		/* Assign the two min - nodes as the left and right child of this node*/
		r = (node*)malloc(sizeof(node));
		r -> left = p;
		r -> right = q;
		f = p -> freq + q -> freq;
		r -> freq = f;
		r -> ch = '\0';
		insertNode(&h, r);
	}
	leafOrder(p);
}
int ind = 0;	/* Temporary variable for indexing str */
char str[64];	/* To store the huffmann codes temporary */
/* This Function visits each leaf node and determines the Huffmann code for each character */
void leafOrder(node *h){
	if(h){
		if(h -> left){
			str[ind++] = '0';
		}
		leafOrder(h -> left);
		if(h -> right){
			str[ind++] = '1';
		}
		leafOrder(h -> right);
		if(h -> left == NULL && h -> right == NULL){
			str[ind] = '\0';
			strcpy(array[h -> ch].str, str);
			array[h -> ch].ch = h -> ch;
		}
		ind --;		//backtrack
	}	
}
/* To initialize the global array */
void initializearray(struct huff *array){
	int i = 0;
	for(i = 0; i < 128; i++){
		strcpy(array[i].str, "");
	}
}
