#define IMAX 100000
typedef struct queue{
	int front, rear;
	int count;
	unsigned int array[IMAX];
}queue;
void qinit(queue *q);
void enqueue(queue *q, unsigned int m);
unsigned int dequeue(queue *q);
int isqEmpty(queue *q);
int isqFull(queue *q);
