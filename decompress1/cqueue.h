#define CMAX 64
typedef struct cqueue{
	int front, rear;
	int count;
	char array[CMAX];
}cqueue;
void cqinit(cqueue *q);
void cenqueue(cqueue *q, char ch);
char cdequeue(cqueue *q);
int cisqEmpty(cqueue *q);
int cisqFull(cqueue *q);
int noOfCharacters(cqueue *q);
void enqueueFromString(cqueue *q, char *str);
unsigned int getIntegerFromQueue(cqueue *q);
unsigned int getIntFromQueue(cqueue *q);
