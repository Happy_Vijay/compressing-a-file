 #include"stack.h"			//This contains all protypes of integer stack
#include"cstack.h"			//This contains all protypes of character stack
#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<string.h>
#define MAX 48				//This is maximum length upto which we could evaluate the infix expression
#define OPERATOR 100			//This are different type the token may contain
#define OPERAND 200
#define ENDD 300
#define ERROR 400
typedef struct token{		
	int type;
	char operator;
	int operand;
}token;
int precedence(char);			//Returns the precedence of an operator
int infix(char *);
int readline(char *, int max);
token gettoken(char *, int *);
int main(){
	int result;
	char line[MAX];
		while(readline(line, MAX)){				//readline function to read the expression
			result = infix(line);
			if(result == INT_MIN){			//Infix Function will return INT_MIN for invalid input
				fprintf(stderr, "Invalid Expression\n");
				exit(1);
			}
			printf("\n%d\n", result);
		}
	return 0;
}			
int infix(char *line){
	int j = 0, x, y, val;
	char ch;
	stack s;		// Stack s to store operands
	static int reset = 0;		// Reset to solve another exp.
	cstack p;		// Stack p to store operators
	token t;		
	init(&s);		// Initializing stacks s and p
	cinit(&p);
	while(1){
	t = gettoken(line, &reset);
	j++;
	if(t.type == ENDD){		// If the stack token returned is ENDD pop all the elements until the expression get evaluated
		while(!cisEmpty(&p)){
			ch = cpop(&p);
			x = pop(&s);
			y = pop(&s);
			switch(ch){
				case '+' : val = y + x;
					break;
				case '-' : val = y - x;
					break;
				case '*' : val = y * x;
					break;
				case '/' : val = y / x;
					break;			
				case '%' : val = y % x;
					break;
			}
			push(&s, val);		// Push the result on stack p
		}
		val = pop(&s);
		reset = 1;			// Reset gettoken()
		return val;			// val is returned by infix
	}
	else if(t.type == OPERAND){
		push(&s, t.operand);		//If the token type is OPERAND, simply push it on stack s
	}
	else if(t.type == OPERATOR){		//If thetoken type is OPERATOR, then
		if(t.operator == '('){		//For left parentheses, push it on stack p
			cpush(&p, t.operator);	
		}
		else if(t.operator == ')'){	//If Right parentheses is encountered then, pop stack p, perform operations until '(' comes 
			if(cisEmpty(&p)){
				fprintf(stderr, "Invalid Infix Expression");
				return INT_MIN;			//If no left parenthes, error
			}
			while((ch = cpop(&p)) != '('){
				x = pop(&s);
				y = pop(&s);
				switch(ch){
					case '+' : val = y + x;
						break;
					case '-' : val = y - x;
						break;
					case '*' : val = y * x;
						break;
					case '/' : val = y / x;
						break;			
					case '%' : val = y % x;
						break;				
				}
				push(&s, val);
				if(cisEmpty(&p)){
					fprintf(stderr, "Invalid Infix Expression");
					return INT_MIN;
				}
			}
		}
		else{					// If the operator is other than the parentheses
			while(1){
				if(!cisEmpty(&p)){
					ch = cpop(&p);	// If the stack is not empty, pop operator to compare precedence
					if(ch == '('){
						cpush(&p, ch);
						cpush(&p, t.operator);
						break;
					}
				}
				else {
					cpush(&p, t.operator);	// Else push the operator
					break;
				}	
				if(precedence(ch) >= precedence(t.operator)){		//Compare the precedence of operators, if stack
					if(!isEmpty(&s))	//p containd operator of higher precedence, then pop it perform operations 
						x = pop(&s);	//and push it back on s
					else{
						fprintf(stderr, "Invalid Infix Expression");
						return INT_MIN;
					}	
					if(!isEmpty(&s))
						y = pop(&s);
					else{
						fprintf(stderr, "Invalid Infix Expression");
						return INT_MIN;
					}	
					switch(ch){
						case '+' : val = y + x;
							break;
						case '-' : val = y - x;
							break;
						case '*' : val = y * x;
							break;
						case '/' : val = y / x;
							break;			
						case '%' : val = y % x;
							break;				
					}
					push(&s, val);
				}
				else{						//Else, if ch is of higher precedence, push it on p
					cpush(&p, ch);
					cpush(&p, t.operator);			// Also, push the token character
					break;
				}
			}
		}
	}
	else if(t.type == ERROR){
		fprintf(stderr, "Invalid Expression\n");
		exit(1);
	}
	}
}		
int precedence(char ch){		/* Here, I have defined some numeric constants for each operators */
	switch(ch){
		case '+' : return 1;
		case '-' : return 1;
		case '*' : return 2;
		case '/' : return 2;		
		case '%' : return 2; 
		}
}
int readline(char *line, int max){	/*Read the characters as specified by user*/
	char ch;
	int i = 0;
	while((ch = getchar()) != EOF){
		if(i == MAX - 1){
			break;
		}
		line[i++] = ch;
	}
	line[i] = '\0';
	return i;
}
enum states {SPACE, OP, NUM, END, ERR};		/* This states specify what operations are to be done */
token gettoken(char *line, int *reset){
	static enum states state = SPACE;
	char ch;
	static int i = 0;
	static char oper = '0';			//To store the operator
	static int num = 0;
	token t;
	if(*reset == 1){
		i = 0;
		*reset = 0;
		state = SPACE;
	}
	while(1){
		ch = line[i];	
		switch(state){
			case NUM:
				switch(ch){
					case '0':case '1':case '2':		//If the state is NUM and you encountered a digit
					case '3':case '4':case '5':
					case '6':case '7':case '8':
					case '9':
						state = NUM;			//Continue Calculation of number
						num = num * 10 + ch - '0';
						i++;
						break;
					case '+':case '-':case '*':		//If the state is NUM and you encountered an operator
					case '/':case '%':case '(':
					case ')':
						oper = ch;
						state = OP;
						i++;
						t.type = OPERAND;
						t.operand = num;
						return t;			//return the number
						break;	
					case '\0':				//If the state is NUM and you encountered end of expression
						state = END;			//return the number
						t.type = OPERAND;
						t.operand = num;
						return t;
						break;
					case ' ':				//If the state is NUM and you encountered a space
						state = SPACE;			
						i++;
						t.type = OPERAND;
						t.operand = num;
						return t;			//return the number
						break;
					default :				// If any other character, Error
						state = ERR;	
				}
				break;		
			case OP :
				switch(ch){
					case '0':case '1':case '2':
					case '3':case '4':case '5':	 //If the state is OP and you encountered a digit
					case '6':case '7':case '8':	//Start calculating the number
					case '9':
						num = 0;
						state = NUM;
						num = num * 10 + ch - '0';
						i++;
						t.type = OPERATOR;
						t.operator = oper;
						return t;
					case '+':case '-':case '*':	//If the state is OP and you encountered an operator, return 
					case '/':case '%':case '(':	// previous operator
					case ')':
						state = OP;
						i++;
						t.type = OPERATOR;	
						t.operator = oper;
						oper = ch;
						return t;	
					case '\0':
						state = END;
						t.type = OPERATOR;
						t.operator = oper;
						return t;		//return operator
					case ' ':
						state = SPACE;
						i++;
						t.type = OPERATOR;
						t.operator = oper;
						return t;		//return operator
					default :
						state = ERR;	
				}
				break;		
			case ERR : 	
					fprintf(stderr, "Invalid expression\n");		
					t.type = ERROR;
					return t;		//Error state
			case END :	t.type = ENDD;
					return t;		//End state
			case SPACE:	
				switch(ch){
					case '0':case '1':case '2':
					case '3':case '4':case '5':
					case '6':case '7':case '8':		//Start calculation of number
					case '9':num = 0;
						state = NUM;
						num = num * 10 + ch - '0';
						i++;
						break;
					case '+':case '-':case '*':
					case '/':case '%':case '(':
					case ')':
						state = OP;			//If operator encountere store it
						i++;
						oper = ch;
						break;
					case '\0':
						state = END;			//If null encountered state = END
						break;
					case ' ':
						state = SPACE;			// Continue in SPACE state
						i++;
						break;
					default :
						state = ERR;			//Any other character is ERROR
				}
		}
	}		
}