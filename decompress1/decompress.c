#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include"cqueue.h"
#define MAX 64
#define SWAP(x, y, t) ((t) = (x), (x) = (y), (y) = (t))
typedef struct node{
	char ch;
	struct node *left, *right;
}node;
/* Global Variables */
int fr;	//File descriptor for reading
int fw;	//File descriptor for writing
struct node *root;
/* Function Prototypes */
void readHeader();	// Reads the header of compressed File, and codes assigned to each character
node* createNode();	// Creates a node
void initroot();	// Initializes the root node
void convertShortIntoString(short sh, int l, char *str);
void leaforder(node *);
void readdata();
void convertIntoString(unsigned int m, unsigned int s, char *str);
int isLeaf(node *);

int main(int argc, char *argv[]){
	if(argc != 3){
		printf("User to enter two files, first to be uncompressed and second that is uncompressed version of first\n");
		exit(1);
	}
	fr = open(argv[1], O_RDONLY);
	if(fr == -1){
		printf("Open Failed");
		exit(1);
	}
	fw = open(argv[2], O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
	if(fw == -1){
		printf("Open Failed");
		exit(1);
	}
	initroot();		//Initialize the root node
	readHeader();
	//leaforder(root);
	readdata();
	return 0;
}
/* This Function reads the header of the compressed file, reads the code 
	for each character and re-builds the corresponding Huffmann's Tree */
void readHeader(){
	int i, j;
	unsigned int size;
	node *currNode = root;
	char ch, len;
	int l;
	short sh;
	char str[MAX];
	read(fr, &size, sizeof(int));
	for(i = 0; i < size; i++){
		read(fr, &ch, 1);
		read(fr, &len, 1);
		read(fr, &sh, sizeof(short));
		l = len;
		convertShortIntoString(sh, l, str);
		j = 0;
		currNode = root;
		while(str[j] != '\0'){
			if(str[j] == '1'){
				if(currNode -> right == NULL){
					currNode -> right = createNode();
				}
				currNode = currNode -> right;
			}
			else{
				if(currNode -> left == NULL){
					currNode -> left = createNode();
				}
				currNode = currNode -> left;
			}
			j++;
		}
		currNode -> ch = ch;
	}
}
int ind = 0;
char str3[32];
void leaforder(node *m){
	if(m == NULL){
		return;
	}
	if(m -> left){
		str3[ind++] = '0';	
	}
	leaforder(m -> left);
	if(m -> right){
		str3[ind++] = '1';	
	}
	leaforder(m -> right);
	if(m -> left == NULL && m -> right == NULL){
		str3[ind] = '\0';
		printf("%c %s\n", m -> ch, str3);
	}
	ind--;
}
/* Function to create a node */
node* createNode(){
	node *temp;
	temp = (node *)malloc(sizeof(node));
	if(!temp){
		return NULL;
	}
	temp -> left = temp -> right = NULL;
	return temp;
}
void initroot(){
	root = createNode();
}
/* Read data from file */
void readdata(){
	int i;
	unsigned int m;
	char ch, wch;
	long long datasize;
	char str[50];
	node *currNode = root;
	cqueue cq;
	unsigned int s;
	cqinit(&cq);
	read(fr, &datasize, sizeof(long long));
	printf("%lld", datasize);
	for(i = 0; i < datasize - 2; i++){
		read(fr, &m, sizeof(int));
		convertIntoString(m, sizeof(int) * 8, str);
		enqueueFromString(&cq, str);
		while(!cisqEmpty(&cq)){
			if(isLeaf(currNode)){
				wch = currNode -> ch;
				write(fw, &wch, 1);
				currNode = root;
			}
			ch = cdequeue(&cq);
			if(ch == '1'){
				currNode = currNode -> right;
			}
			else{
				currNode = currNode -> left;
			}
		}
	}
	read(fr, &s, sizeof(int));
	read(fr, &m, sizeof(int));
	convertIntoString(m, s, str);
	enqueueFromString(&cq, str);
	while(!cisqEmpty(&cq)){
			if(isLeaf(currNode)){
				wch = currNode -> ch;
				write(fw, &wch, 1);
				currNode = root;
			}
			ch = cdequeue(&cq);
			if(ch == '1'){
				currNode = currNode -> right;
			}
			else{
				currNode = currNode -> left;
			}
	}
}
/* Converts short into string */
void convertShortIntoString(short sh, int l, char *str){
	int i = 0;
	int l1, diff;
	char temp;
	char str1[MAX];
	while(sh){
		if(sh % 2){
			str[i] = '1';
		}
		else{
			str[i] = '0';
		}
		i++;
		sh = sh / 2;
	}
	str[i] = '\0';
	l1 = strlen(str);
	/* Reverse string */
	for(i = 0; i < l1 / 2; i++){
		SWAP(str[i], str[l1 - 1 - i], temp);
	}
	diff = l - l1;
	for(i = 0; i < diff; i++){
		str1[i] = '0';
	}
	str1[i] = '\0';
	strcat(str1, str);
	strcpy(str, str1);
}
void convertIntoString(unsigned int m, unsigned int s, char *str){
	int i = 0, l, diff;
	char temp;
	char str1[40];
	while(m){
		if(m % 2){
			str[i] = '1';
		}
		else{
			str[i] = '0';
		}
		i++;
		m = m / 2;
	}
	str[i] = '\0';
	l = strlen(str);
	for(i = 0; i < l / 2; i++){
		SWAP(str[i], str[l - 1 - i], temp);
	}
	diff = s - l;
	for(i = 0; i < diff; i++){
		str1[i] = '0';
	}
	str1[i] = '\0';
	strcat(str1, str);
	strcpy(str, str1);
}
int isLeaf(node *m){
	return(m -> left == NULL && m -> right == NULL);
}
