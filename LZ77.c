#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include"circularq.h"
#include<fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#define NIL 0xFFFF
/* Minimum match length is given by Threshold */
#define THRESHOLD 2
#define DICTBITS  13
#define HASHBITS  10
#define DICTSIZE  (1 << DICTBITS)
#define HASHSIZE  (1 << HASHBITS)
#define SHIFTBITS ((HASHBITS + THRESHOLD) / (THRESHOLD + 1))
#define HASHFLAG1 0x8000
#define HASHFLAG2 0X7FFF
#define SECTORBIT 10
#define SECTORLEN (1 << SECTORBIT)
/* Global Variables */
unsigned char dictionary[DICTSIZE];
unsigned int hash[HASHSIZE], nextlink[DICTSIZE], lastlink[DICTSIZE];
unsigned int dictpos;
int fr;		//File for reading
FILE *fw;
void hashData(unsigned int dictpos, unsigned int bytestodo);
void initencode();
void deleteData(unsigned int);
unsigned int getMatch(int i, int *prevpos, int l);
unsigned int readdata();
int main(int  argc, char *argv[]){
	unsigned int l;
	unsigned int i, pos, prevpos, k;
	int count = 0;
	unsigned int matchlen = 0, matchlen1 = 0;
	queue q;
	triplet t;
	init(&q);
	fr = open(argv[1], O_RDONLY);
	if(fr == -1){
		printf("Opening file Failed\n");
		exit(1);
	}
	fw = fopen(argv[2], "wb");
	initencode();
	while((l = readdata())){
		hashData(0, l);
		for(i = 0; i < l - THRESHOLD; i++){
			if(nextlink[i] != NIL){
				/* Try for better match */
				pos = nextlink[i];
				/*k = 0;
				while(dictionary[pos + k ] == dictionary[i + k]){
					k++;
				}
				matchlen = k;
				i = i + matchlen;
				t.ch = dictionary[i];
				if(k == 0){
					t.pos = 0;
				}
				else{
					t.pos = pos;
				}	
				t.length = matchlen;*/
				matchlen = getMatch(i, &prevpos, l);
				while((matchlen1 = getMatch(i + 1, &prevpos, l)) > matchlen){
					matchlen = matchlen1;
					pos = prevpos;
					t.ch = dictionary[i];
					t.pos = 0;
					t.length = 0;
					enqueue(&q, t);
					i++;
				}
				i = i + matchlen;
				if(matchlen == 0){
					t.pos = 0;
				}
				else{
					t.pos = pos;
				}
				t.length = matchlen;
				t.ch = dictionary[i];
			}
			else{
				t.ch = dictionary[i];
				t.length = 0;
				t.pos = 0;
			}
			enqueue(&q, t);
		}
	while(!isqEmpty(&q)){
		t = dequeue(&q);
		fwrite(&t, sizeof(triplet), 1, fw);
		count++;
	}
	
	deleteData(l);
	}
}
void initencode(){
	int i;
	for (i = 0; i < HASHSIZE; i++) 
  		hash[i] = NIL;
}
void hashData(unsigned int dictpos, unsigned int bytestodo){
	register unsigned int i, j, k;
	if(bytestodo <= THRESHOLD)
		for (i = 0; i < bytestodo; i++)
			nextlink[dictpos + i] = lastlink[dictpos + i] = NIL;
	else{
	/* No matches for last threshold bytes allowed*/
		for (i = bytestodo - THRESHOLD; i < bytestodo; i++){
			nextlink[dictpos + i] = lastlink[dictpos + i] = NIL;
		}
		j = (((unsigned int)dictionary[dictpos]) << SHIFTBITS) ^ dictionary[dictpos + 1];
		k = dictpos + bytestodo - THRESHOLD;  /* calculate end of sector */
		for (i = dictpos; i < k; i++){
			lastlink[i] = (j = (((j << SHIFTBITS) & (HASHSIZE - 1)) ^ dictionary[i + THRESHOLD])) | HASHFLAG1;
			if((nextlink[i] = hash[j]) != NIL) lastlink[nextlink[i]] = i;
				hash[j] = i;
			}
	}
}
unsigned int readdata(){
	unsigned int len;
	len = read(fr, dictionary, SECTORLEN);
	return len;
}
void deleteData(unsigned int maxlength){
	register unsigned int i, j;
	for (i = 0; i < maxlength; i++){
		if((j = lastlink[i]) & HASHFLAG1){
			if(j != NIL) hash[j & HASHFLAG2] = NIL;
		}
		else
			nextlink[j] = NIL;
  	}
}
unsigned int getMatch(int i, int *prevpos, int l){
	int k = 0;
	int pos = nextlink[i];
	if(pos == NIL){
		return 0;
	}
	while((i + k < l - THRESHOLD) && (dictionary[pos + k] == dictionary[i + k])){
		k++;
	}
	*prevpos = pos;
	return k;
}
