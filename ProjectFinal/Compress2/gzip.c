/* The Gzip algorithm is based on DEFLATE: It comprises of two parts :
	Part a) Generation of triplets <pos length character> where pos is closest and length is maximum. This is part of LZ77 Sliding dictionary Compression Algorithm
	Part b) Compression of this triplets by Huffman's Coding by Algoritm (I) 
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include"circularq.h"
#include<fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include<limits.h>
/* Minimum match length is given by Minlen */
#define MINLEN 2
/* Since the hash function considers only first six bits of three chracters, the maximum hash value can be
	111111 111111 111111 i.e 2 to the power 19 - 1 */
#define SIZE 1 << 19
/* For less collision the size of dictionary must be less than SIZE */
#define DICTIONARYSIZE 1 << 13
/* Declarartion of global arrays */
unsigned char dictionary[DICTIONARYSIZE];		/* This is the siliding dictionary */
unsigned int hash[SIZE]			/* The hash Table */
, next[DICTIONARYSIZE]				/* Stores the occurence of characters repeated  */
, prev[DICTIONARYSIZE];				/* This is typically used to reinitialize the hash array after a part of file is read */
int fr1;				/* File for reading */
int fw1;				/* File For Writing */
void hashData(unsigned int pos, unsigned int len);		/* The hash function used for searching the string */
void initializeHashArray();		/* Initialize the Hash Array for first time */			
void reinitializeHashArray(unsigned int);	/*This Function reinitializes the global arrays next, prev and  hash, So that next instance of file can be loaded and hashed because file size may be larger*/ 
void encodebyHuffman(char *);		/* After we have completed writing triplets <pos, len ,ch> in file encode the file by Huffman */
unsigned int getMatch(int i, int *prevpos, int l);
unsigned int readdata();		/* Reads the data from file */
int main(int  argc, char *argv[]){
	unsigned int l;		/* l is number of characters read and stores in dictionary*/
	unsigned int i, pos, prevpos, k;	/*pos and prevpos are posritions of previous occurence of strings*/
	unsigned int count = 0;		/* count keeps the update of number of triplets enqueued*/
	unsigned int matchlen = 0, matchlen1 = 0;
	lqueue q;		/* This queue enqueues the triplets */
	triplet t;		/* Declaration of struct triplet in cqueue.h */
	linit(&q);
	/* File Handling */
	if(argc != 3){
		printf("Invalid arguments to main\n");
		printf("<./gzip> <file to be compressed> <name of after compressed file>\n");
		exit(1);
	}
	fr1 = open(argv[1], O_RDONLY);
	if(fr1 == -1){
		printf("Opening file Failed\n");
		exit(1);
	}
	fw1 = open("temp", O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);	/* The temp file stores the triplets */
	if(fw1 == -1){
		printf("Opening file Failed\n");
		exit(1);
	}
	initializeHashArray();		/* Initialize the Hash Array */
	while((l = readdata())){	/* While there are non-zero "l" characters in dictionary */
		hashData(0, l);		/* Hash the characters in the dictionary upto "l" */
		for(i = 0; i < l; i++){
			if(next[i] != INT_MAX){
/* If next[i] is not INT_MAX this means that the string is present in search buffer, Now Try for better match */
				pos = next[i];
				matchlen = getMatch(i, &prevpos, l);
				while((matchlen1 = getMatch(i + 1, &prevpos, l)) > matchlen){	/* While we get a better match length*/
					matchlen = matchlen1;
					pos = prevpos;		/* Update the values of pos and matchlen */
					t.ch = dictionary[i];
					t.pos = 0;
					t.length = 0;
					lenqueue(&q, t);	/* The current character triplet is enqueued as it is */
					count++;
					i++;
				}
				i = i + matchlen;		/* Increment the dictionary pos by match length */
				if(matchlen == 0){		/* Resolution of collision */
					t.pos = 0;
				}
				else{
					t.pos = pos;
				}
				t.length = matchlen;
				t.ch = dictionary[i];
			}
			else{
				t.ch = dictionary[i];		/* If next[i] is INT_MAX i.e no matches for the character */
				t.length = 0;
				t.pos = 0;
			}
			lenqueue(&q, t);		/* Enqueue the triplet */
			count++;
		}
		write(fw1, &count, sizeof(int));		/* Write the count integer, first*/
		count = 0;
		while(!lisqEmpty(&q)){				/* Write all the triplets enqueued in queue */
			t = ldequeue(&q);
			write(fw1, &t, sizeof(triplet));
		}
	/* End of one part of file, if the file is still longer repeat this whole process while we reach the end of file */
		reinitializeHashArray(l);
	}
	/* Done with writing the triplets */
	close(fw1);	/*  Closing temp file saves it on disk*/
	/* PART B : Encoding the triplets produced by LZ77 by Huffman*/
	encodebyHuffman(argv[2]);	/* Now on this triplet-structured file we use Huffman Coding, which we did in Algorithm (I) */
	remove("temp");			/* Since the work of temp file is over remove it */
	return 0;
}
/* Function that initializes the Hash Array for first time */
void initializeHashArray(){
	int i;
	for (i = 0; i < SIZE; i++) 
  		hash[i] = INT_MAX;
}
/* Hashing Function */
/* Basically this Hash Function considers only last six bits in binary represent of ASCII value of character in dictionary. 
	Suppose that we want to hash the string "abr" then 
	a = 01  100001
	b = 01  100010
	r = 01  110010
	So we obtain hash value as 100001 100010 110010 i.e 1,37,394 in decimal. This is guranteed to be non - negative
	but collision is not resolved completely and we will check for it.
*/
void hashData(unsigned int pos, unsigned int length){
	register unsigned int i, j, k;
	/* No further matches */
	if(length <= MINLEN)
		for (i = 0; i < length; i++)
			next[pos + i] = prev[pos + i] = INT_MAX;
	else{
	/* No matches for last MINLEN bytes allowed*/
		for (i = length - MINLEN; i < length; i++){
			next[pos + i] = prev[pos + i] = INT_MAX;
		}
		for(i = pos; i < pos + length - MINLEN; i++){
			j = (((dictionary[i] & 63) << 12) | ((dictionary[i + 1] & 63) << 6) | ((dictionary[i + 2] & 63)));
			prev[i] = j;		/* Prev[i] stores the hash values */
			next[i] = hash[j];
			hash[j] = i;
		}
	}
}
/* Read data from file in dictionary array */
unsigned int readdata(){
	unsigned int len;
	len = read(fr1, dictionary, 2 << 10);
	return len;
}
void reinitializeHashArray(unsigned int maxlength){
	register unsigned int i, j;
  	for(i = 0; i < maxlength - MINLEN; i++){
  		j = prev[i];			/* Since all the hash buckets aren't used we initialize only those indexes that  were actually used in prev hash Function and make them INT_MAX THis will save the time conssiderable on expense of a global array prev[SIZE] */
  		hash[j] = INT_MAX;
  		next[i] = INT_MAX;
  	}
}
/* Sends the match*/
unsigned int getMatch(int i, int *prevpos, int l){
	int k = 0;
	int pos = next[i];
	/* It may be possible that the string is not present and next[i] is INT_MAX */
	if(pos == INT_MAX){
		return 0;
	}
	while((i + k < l - MINLEN) && (dictionary[pos + k] == dictionary[i + k]) && (pos + k < i)){
		/* No array index violation and checking for equality*/
		k++;
	}
	*prevpos = pos;
	if(k < MINLEN){		/*Matches we length less than MINLEN not allowewd*/
		return 0;
	}
	return k;
}
