#define QMAX 10000
typedef struct triplet{
	unsigned short pos;
	unsigned char length;
	char ch;
}triplet;
typedef struct lqueue{
	triplet array[QMAX];
	int i, j, count;
}lqueue;
void linit(lqueue *q);
void lenqueue(lqueue *q, triplet t);
triplet ldequeue(lqueue *q);
int lisqEmpty(lqueue *q);
int lisqFull(lqueue *q);
