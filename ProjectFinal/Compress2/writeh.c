#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<stdlib.h>
#include<string.h>
#include"htree.h"
#include<math.h>
#include"queue.h"	//For Integer Queue
#include"cqueue.h"	//For Character Queue
/* Global Variables Declarations */

unsigned int size;		//Stores Number of distinct characters in File
struct huff array[256];	//Global array that will contain huffman's code for all characters.
int fr;				// File descriptor of orignal file
int fw;				// File descriptor for compressed file

/* Function Prototypes */

void initializearray(struct huff *array);
void huffman(unsigned long long *ascii);	//This Function builds a huffman tree, assigns Huffman code for each character
void writeHeader();		// To write a header that desribes huffmann codes
unsigned int convertStringtoint(unsigned char *str); 
void writeData();		// Writes data in compressed Fashion
void getHuffmanCode(unsigned char ch, unsigned char *str);
void leafOrder(node *h);	// Function to visit each leaf node recursively
/* Encode the filename (here argv[2] by producing its Huffmann code) */
void encodebyHuffman(char *filename){
	unsigned char ch;
	int i;
	int count = 0;
	unsigned long long ascii[256] = {0};
	bzero(ascii, 256 * sizeof(unsigned long long));
	/* Open the temp file of LZ77 */
	fr = open("temp", O_RDONLY);
	if(fr == -1){
		printf("Open1 Failed");
		exit(1);
	}
	while(read(fr, &ch, 1)){
		count++;
		(ascii[ch])++;
	}
	fw = open(filename, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
	if(fw == -1){
		printf("Open2 Failed %s\n", filename);
		exit(1);
	}
	/* This Function builds a huffman tree, assigns Huffman code for each character and stores them in array */
	huffman(ascii);
	/* Determine number of distinct characters */	
	for(i = 0; i < 256; i++){
		if(strlen(array[i].str)){
			size++;
		}
	}
	/* Since we read the complete file to determine frequency of all characters again lseek the file descriptor to start */
	lseek(fr, SEEK_SET, 0);
	/* Write a header to describe the Huffman tree */
	writeHeader();
	/* Write the data(Huffman code for each character) in form of cascaded integers */
	writeData();
}
/* My header is of form
 <char ch>     |	   <char len>            |                <int sh>
 for character |	 length of huffmann code|		Huffmann Code
*/
void writeHeader(){
	int i;
	unsigned char ch, len;
	unsigned int sh;
	write(fw, &size, sizeof(unsigned int));
	for(i = 0; i < 256; i++){
		if(strlen(array[i].str)){		// Write only those characters that exits in the File
			ch = array[i].ch;
			write(fw, &ch, 1);
			len = strlen(array[i].str);
			write(fw, &len, 1);
			sh = convertStringtoint(array[i].str);
			write(fw, &sh, sizeof(unsigned int));
		}
	}
}
/* This Function converts The longer string of Huffman's code into shorter Integers, so that less space is required */
unsigned int convertStringtoint(unsigned char *str){
	unsigned int sh = 0;
	int i, l = strlen(str);
	for(i = 0; i < l; i++){
		if(str[i] == '1'){
			sh = sh + pow(2, l - 1 - i);
		}
	}
	return sh;
}
/* This Function writes characters in file by using variable Huffman's Code */
/* The variable codes are cascaded into a single unsigned integers using binary conversions and queues */
void writeData(){
	unsigned char ch;
	unsigned char str[64];
	unsigned int m;
	int flag = 0, flag2 = 0;
	unsigned long long datasize = 0, bytes = 0;
	cqueue cq;
	queue q;
	cqinit(&cq);
	qinit(&q);
	while(read(fr, &ch, 1)){
		getHuffmanCode(ch, str);
		enqueueFromString(&cq, str);
		if(noOfCharacters(&cq) >= 32){
			m = getIntegerFromQueue(&cq);
			enqueue(&q, m);
			datasize++;
		}
	}
	m = noOfCharacters(&cq);
	enqueue(&q, m);
	m = getIntFromQueue(&cq);
	enqueue(&q, m);
	datasize += 2;
		write(fw, &datasize, sizeof(unsigned long long));
		while(!isqEmpty(&q)){
			m = dequeue(&q);
			write(fw, &m, sizeof(unsigned int));
		}
}	
void getHuffmanCode(unsigned char ch, unsigned char *str){
	strcpy(str, array[ch].str);
}
void huffman(unsigned long long *ascii){
	int i;
	unsigned long long f;
	htree h;
	node *p, *q, *r;
	init(&h);
	/* Create a node for each character present in file and insert it into min-heap */
	for(i = 0; i < 256; i++){
		if(ascii[i]){
			createAndInsertNode(&h, ascii[i], i + '\0'); //Take only non- zero frequency characters
		}	
	}
	/* Now that we have created a min-heap, we are ready to build the huffman tree */
	while(1){
		/* Remove two nodes from min-heap */
		if(!isTreeEmpty(&h)){
			p = removeNode(&h);
		}
		else{
			break;
		}
		if(!isTreeEmpty(&h)){
			q = removeNode(&h);
		}
		/*If min heap has only one node left, we are done*/
		else{
			break;
		}
		/* Create a node with freq. equal to sum of the two min - nodes and add it again two min heap */
		/* Assign the two min - nodes as the left and right child of this node*/
		r = (node*)malloc(sizeof(node));
		r -> left = p;
		r -> right = q;
		f = p -> freq + q -> freq;
		r -> freq = f;
		r -> ch = '\0';
		insertNode(&h, r);
	}
	leafOrder(p);
}
/* Initialize the array , such that all Huffman code strings are empty */
void initializearray(struct huff *array){
	register int i = 0;
	for(i = 0; i < 256; i++){
		strcpy(array[i].str, "");
	}
}
int ind = 0;	/* Temporary variable for indexing str */
unsigned char str[64];	/* To store the huffmann codes temporary */
/* This Function visits each leaf node and determines the Huffmann code for each character */
void leafOrder(node *h){
	if(h){
		if(h -> left){
			str[ind++] = '0';
		}
		leafOrder(h -> left);
		if(h -> right){
			str[ind++] = '1';
		}
		leafOrder(h -> right);
		if(h -> left == NULL && h -> right == NULL){
			str[ind] = '\0';
			strcpy(array[h -> ch].str, str);
			array[h -> ch].ch = h -> ch;
		}
		ind --;		//backtrack
	}	
}
