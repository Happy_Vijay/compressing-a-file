Title : Compression of File
Mis No: 111703040
Name  : Vijay Jagdish Mundra
Description :
	I implemented two algorithms.
	a) Huffman Coding
	b) Gzip
a) Huffman Coding : 
	Part 1 ( Compression)
I generated the huffman codes with help of min-heap. Now we have Huffman code for all the characters in the file in the Huffman tree. To write them I used a queue to enqueue the '0's and' 1's of successive characters when they become 32 , I convert them into integers and write these Integers in the file. This forms by compressed file. Suitable header is written in the file that describes the Huffman tree.
	Part 2 (Decompression)
By reading the header, the function readHeader rebuilds the Huffman tree. So now read the integers in the file and obtain the Huffman codes sequences. Search for the character which matches with the Huffman code in the Tree built. Write this characters back in the file to obtain decompressed File.

b) Gzip :
	Part a) (Compression)
This can be broadly described in two steps:
	Step 1) LZ77 : Producing triplets in form of <pos len character>.
		Do this I read the characters of file in dictionary, and Hash all the strings with length 3 in dictionary. After hashing I search for the repeated occurence of a string, if the string is not repeated the triplet <0 0 ch> is enqueued.
	But if string has a previous occurence then the triplet <closestpos(of occurence), matchlength, nextcharacter> is written in the file.
	If the file size is big then perhaps the dictionary size may fall short so the triplets are written in paragraphs.
	Step 2) Compressing this triplet file by Huffman.
	
	Part b) (Decompression)
This also involves two steps:
	Step 1) Decoding the Huffman's Code for triplets.
	Step 2) LZ77 Decompression : This triplets are read, if pos and matchlen are 0 then the characters are written as it is in the sliding dictionary, else matchlen character from pos in the dictionary are written and then this character is written. Finally this dictionary is written in the file. Same process is repeated with all the paragraphs.
