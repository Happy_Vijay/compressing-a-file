#define CMAX 64
typedef struct cqueue{
	int front, rear;
	int count;
	unsigned char array[CMAX];
}cqueue;
void cqinit(cqueue *q);
void cenqueue(cqueue *q, unsigned char ch);
unsigned char cdequeue(cqueue *q);
int cisqEmpty(cqueue *q);
int cisqFull(cqueue *q);
int noOfCharacters(cqueue *q);
void enqueueFromString(cqueue *q, unsigned char *str);
unsigned int getIntegerFromQueue(cqueue *q);
unsigned int getIntFromQueue(cqueue *q);
