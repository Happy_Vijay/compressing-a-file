/* The decompression process involves :
	Part a) Decompressing the huffman coded file and obtaining the triplet-structured <pos len char> file from it.
	Part b) Building back the dictionary obtained from the triplets.
	Part c) Writing this dictionary in decompressed file  
*/
#include<stdio.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/stat.h>
#include<stdlib.h>
int fr1, fw1;
typedef struct triplet{
	unsigned short pos;
	unsigned char length;
	char ch;
}triplet;
triplet *dictionary;	/* To read the triplets of one paragraph*/
#define DICTIONARYSIZE 1 << 13
unsigned char chararray[DICTIONARYSIZE];		// Dictionary array which is going to store all charactes of one paragraph
void decompressbyHuffman(char *);	/* To  decompress the huffman code file */
void decompress();			/* To decode the LZ77 triplets */
int main(int argc, char *argv[]){
	if(argc != 3){
		printf("Invalid Arguments to main\n");
		printf("<./decompress2> <compressed file> <decompress file>\n");
		exit(1);
	}
	decompressbyHuffman(argv[1]);	/* This will also create temp file with triplets in it */
	fw1 = open(argv[2], O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
	if(fw1 == -1){
		exit(1);
	}
	fr1 = open("temp", O_RDONLY);
	if(fr1 == -1){
		exit(1);
	}
	decompress();
	remove("temp");
	return 0;
}
/* To decode the LZ77 triplets */	
void decompress(){
	unsigned int l;
	register unsigned int i, m;
	int flag;
	int prevlen = 0;
	unsigned int k;
	triplet t;
	while(1){
		flag = read(fr1, &l, sizeof(int));
		if(flag == 0){
			/* No more triplets left */
			break;
		}
		else{
			k = 0;
			/* Allocate the dictionary array for first time */
			if(prevlen == 0){
				dictionary = (triplet *)malloc(sizeof(triplet) * l);
			}
			/* If no of structures are more than capacity of dictionary, realloc it*/
			else if(l > prevlen){
				
				dictionary = realloc(dictionary, sizeof(triplet) * l);
			}
			/* If realloc fails */
			if(!dictionary){
				break;
			}
			read(fr1, dictionary, sizeof(triplet) * l);
			for(i = 0; i < l; i++){
				t = dictionary[i];
				if(t.length == 0){
					chararray[k++] = t.ch;
				}
				else{
					/* If match is present then pos != 0 and write thosse many characters from that pos */
					for(m = t.pos; m < t.pos + t.length; m++){
						chararray[k++] = chararray[m];
					}
					chararray[k++] = t.ch;
				}
			}
			/*Write the character array in the decompressed file */
			write(fw1, chararray, k);
			/*Update value of prevlen to l*/
			prevlen = l;
		}
	}
}
