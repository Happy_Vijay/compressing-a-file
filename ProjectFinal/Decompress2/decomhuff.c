#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include"cqueue.h"
#define MAX 64
#define SWAP(x, y, t) ((t) = (x), (x) = (y), (y) = (t))
typedef struct node{
	unsigned char ch;
	struct node *left, *right;
}node;
/* Global Variables */
int fr;	//File descriptor for reading
int fw;	//File descriptor for writing
struct node *root;
/* Function Prototypes */
void readHeader();	// Reads the header of compressed File, and codes assigned to each character
node* createNode();	// Creates a node
void initroot();	// Initializes the root node
void leaforder(node *);
void readdata();
void convertIntoString(unsigned int m, unsigned int s, unsigned char *str);
int isLeaf(node *);
/* filename is argv[2] from gzip */
void decompressbyHuffman(char *filename){
	fr = open(filename, O_RDONLY);
	if(fr == -1){
		printf("Open Failed");
		exit(1);
	}
	/* "temp" is the file which is going to contain the triplets of LZ77*/
	fw = open("temp", O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
	if(fw == -1){
		printf("Open Failed");
		exit(1);
	}
	initroot();		//Initialize the root node
	readHeader();		// reads the header for Huffman
	readdata();
}
/* This Function reads the header of the compressed file, reads the code 
	for each character and re-builds the corresponding Huffmann's Tree */
void readHeader(){
	int i, j;
	unsigned int size;
	node *currNode = root;
	unsigned char ch, len;
	unsigned uch;
	int l;
	unsigned int sh;
	unsigned char str[MAX];
	/* Size describes number of distinct characters in the huffman tree */
	read(fr, &size, sizeof(unsigned int));
	for(i = 0; i < size; i++){
	/* uch : character len, length of huffman code,  sh : huffman code converted into decimal integer */
		read(fr, &uch, 1);
		read(fr, &len, 1);
		read(fr, &sh, sizeof(unsigned int));
		l = len;
		convertIntoString(sh, l, str);
		j = 0;
		/*Reinitialize the node to root every time*/
		currNode = root;
		/* Rebuild the huffman tree from its huffman code*/
		/* For '1' traverse Right */
		/* For '0' traverse Left  */
		while(str[j] != '\0'){
			if(str[j] == '1'){
				if(currNode -> right == NULL){
					currNode -> right = createNode();
				}
				currNode = currNode -> right;
			}
			else{
				if(currNode -> left == NULL){
					currNode -> left = createNode();
				}
				currNode = currNode -> left;
			}
			j++;
		}
		/* Store the character at leaf node */
		ch = uch;
		currNode -> ch = ch;
	}
}
/* Function to create a node */
node* createNode(){
	node *temp;
	temp = (node *)malloc(sizeof(node));
	if(!temp){
		exit(1);
	}
	temp -> left = temp -> right = NULL;
	return temp;
}
/* Initializes the root node of the heap tree */
void initroot(){
	root = createNode();
}
/* Read data from file */
/* Reads the integers and converts them bach into their original characters */
void readdata(){
	int i;
	unsigned int m;
	unsigned char ch, wch;
	long long datasize;
	unsigned char str[50];
	int count = 0;
	node *currNode = root;
	cqueue cq;	/* To give flexibility in conversion of this integers into strings */
	unsigned int s;
	cqinit(&cq);
	/* datasize is the no.of such integers*/
	read(fr, &datasize, sizeof(unsigned long long));
	for(i = 0; i < datasize - 2; i++){
		/* Read the integers one-by-one */
		if(!read(fr, &m, sizeof(unsigned int))){
			break;
		}
		convertIntoString(m, sizeof(unsigned int) * 8, str);
		/* Enqueue this 0's and 1's from string in queue */
		enqueueFromString(&cq, str);
		/* Traverse the huffman tree until leaf node is reached, in this process if we traverse to right '1' is enqueued and if we traverse towards left '0' is enqueued thus we obtain the character from given huffman code */
		while(!cisqEmpty(&cq)){
			if(!currNode){
				break;
			}
			if(isLeaf(currNode)){
				wch = currNode -> ch;
				write(fw, &wch, 1);
				currNode = root;
			}
			ch = cdequeue(&cq);
			if(ch == '1'){
				if(currNode)
					currNode = currNode -> right;
			}
			else{
				if(currNode)
					currNode = currNode -> left;
			}
		}
		count++;
	}
	/* The last two integers are special */
	read(fr, &s, sizeof(unsigned int));
	read(fr, &m, sizeof(unsigned int));	/* 's' tells the no. of active bits in m */
	convertIntoString(m, s, str);
	enqueueFromString(&cq, str);
	/* Repeat the same process has above with this two integers */
	while(!cisqEmpty(&cq)){
			if(isLeaf(currNode)){
				wch = currNode -> ch;
				write(fw, &wch, 1);
				currNode = root;
			}
			ch = cdequeue(&cq);
			if(ch == '1'){
				currNode = currNode -> right;
			}
			else{
				currNode = currNode -> left;
			}
	}
	/* Since the queue can becomes empty before we reach the leaf we need to handle this case*/
	if(currNode){
		wch = currNode -> ch;
		write(fw, &wch, 1);
	}
}
/* Converts int into string */
/* This conversion is nothing but conversion of integer in its Binary equivalent */
void convertIntoString(unsigned int m, unsigned int s, unsigned char *str){
	int i = 0, l, diff;
	unsigned char temp;
	unsigned char str1[40];
	while(m){
		if(m % 2){
			str[i] = '1';
		}
		else{
			str[i] = '0';
		}
		i++;
		m = m / 2;
	}
	str[i] = '\0';
	l = strlen(str);
	for(i = 0; i < l / 2; i++){
		SWAP(str[i], str[l - 1 - i], temp);
	}
	diff = s - l;
	for(i = 0; i < diff; i++){
		str1[i] = '0';
	}
	str1[i] = '\0';
	strcat(str1, str);
	strcpy(str, str1);
}
/* Function which tells that the node is root or not */
int isLeaf(node *m){
	return(m -> left == NULL && m -> right == NULL);
}
