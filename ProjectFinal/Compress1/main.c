#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<stdlib.h>
#include<string.h>
#include"htree.h"
#include<math.h>
#include"queue.h"	//For Integer Queue
#include"cqueue.h"	//For Character Queue
/* Global Variables Declarations */
unsigned int size;		//Stores Number of distinct characters in File
extern struct huff array[256];	//Global array that will contain huffman's code for all characters.
int fr;				// File descriptor of orignal file
int fw;				// File descriptor for compressed file
/* Function Prototypes */
void huffman(unsigned long long *ascii);	//This Function builds a huffman tree, assigns Huffman code for each character
void writeHeader();		// To write a header that desribes huffmann codes
unsigned int convertStringtoint(unsigned char *str); // Converts the code string of long length into integer
void writeData();		// Writes data in compressed Fashion
void getHuffmanCode(unsigned char ch, unsigned char *str);
int main(int argc, char *argv[]){
	unsigned char ch;
	int i;
	unsigned long long ascii[256] = {0};
	bzero(ascii, 256 * sizeof(unsigned long long));
	if(argc != 3){
		printf("User to enter two files, first to be compressed and second that is compressed version of first\n");
		exit(1);
	}
	fr = open(argv[1], O_RDONLY);
	if(fr == -1){
		printf("Open1 Failed");
		exit(1);
	}
	/*Count frequency of each character in array ascii*/
	while(read(fr, &ch, 1)){
		(ascii[ch])++;
	}
	fw = open(argv[2], O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
	if(fw == -1){
		printf("Open2 Failed %s\n", argv[2]);
		exit(1);
	}
	/* This Function builds a huffman tree, assigns Huffman code for each character and stores them in array */
	huffman(ascii);
	/* Determine number of distinct characters */	
	for(i = 0; i < 256; i++){
		if(strlen(array[i].str)){
			size++;
		}
	}
	/*Since we have read the file completely*/
	lseek(fr, SEEK_SET, 0);
	/*Write a header that describes the Huffman tree*/
	writeHeader();
	/* Write Huffman code of each character in form of cascaded integers */
	writeData();
	return 0;
}
/* My header is of form
 <char ch>     |	   <char len>            |                <int sh>
 for character |	 length of huffmann code|		Huffmann Code
*/
void writeHeader(){
	int i;
	unsigned char ch, len;
	unsigned int sh;
	write(fw, &size, sizeof(unsigned int));
	for(i = 0; i < 256; i++){
		if(strlen(array[i].str)){		// Write only those characters that exits in the File
			ch = array[i].ch;
			write(fw, &ch, 1);
			len = strlen(array[i].str);
			write(fw, &len, 1);
			sh = convertStringtoint(array[i].str);
			write(fw, &sh, sizeof(unsigned int));
		}
	}
}
/* Conversion of strings into int just as conversion of binary no. into decimal */
unsigned int convertStringtoint(unsigned char *str){
	unsigned int sh = 0;
	int i, l = strlen(str);
	for(i = 0; i < l; i++){
		if(str[i] == '1'){
			sh = sh + pow(2, l - 1 - i);
		}
	}
	return sh;
}
/* This Function writes characters in file by using variable Huffman's Code */
/* The variable codes are cascaded into a single unsigned integers using binary conversions and queues */
/* Suppose huffman codes for 	a is 10
				b is 001
				c is 11100100
				d is 0011
				e is 011001
				and so on 
This Function cascades this code as a single unsigned Integer 100011110010000101100101111100 and write this Integer into file
*/
void writeData(){
	unsigned char ch;
	unsigned char str[64];
	unsigned int m;
	int flag = 0, flag2 = 0;
	unsigned long long datasize = 0, bytes = 0;
	cqueue cq;	/* Character queue for storing 0's and 1's it gives flexibility in forming integers of fixed 32 bit */
	queue q;	/* For storing the integers */
	cqinit(&cq);
	qinit(&q);
	/* Read characters from file till EOF is reached */
	while(read(fr, &ch, 1)){
		/*Copy huffman code of character in str*/
		getHuffmanCode(ch, str);
		enqueueFromString(&cq, str);	/* enqueue characters from str in queue */
		if(noOfCharacters(&cq) >= 32){	/* Form a integer from first 32 characters*/
			m = getIntegerFromQueue(&cq);
			enqueue(&q, m);		/* Enqueue this integer in integer queue */
			datasize++;
		}
	}
	/* Since characters in queue after EOF is reached may be less than 32*/
	/* We have to write a special integer which describes no. of active bits in last integer*/
	m = noOfCharacters(&cq);
	enqueue(&q, m);
	/* The last integer */
	m = getIntFromQueue(&cq);
	enqueue(&q, m);
	datasize += 2;
	/* datasize is the number of such integers */
	write(fw, &datasize, sizeof(unsigned long long));
	/* Write all this Integers in the file */
	while(!isqEmpty(&q)){
		m = dequeue(&q);
		write(fw, &m, sizeof(unsigned int));
	}
}
/*Gives the huffman code stored in array*/	
void getHuffmanCode(unsigned char ch, unsigned char *str){
	strcpy(str, array[ch].str);
}
