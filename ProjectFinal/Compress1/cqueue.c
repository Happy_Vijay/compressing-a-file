#include"cqueue.h"
#include<string.h>
#include<stdlib.h>
#include<math.h>
void cqinit(cqueue *q){
	q -> count = q -> rear = q -> front = 0;
}
void cenqueue(cqueue *q, unsigned char ch){
	q -> array[q -> rear] = ch;
	q -> rear = (q -> rear + 1) % CMAX;
	q -> count++;
}
unsigned char cdequeue(cqueue *q){
	unsigned char ch;
	ch = q -> array[q -> front];
	q -> front = (q -> front + 1) % CMAX;
	q -> count--;
	return ch;
}
int cisqEmpty(cqueue *q){
	return (q -> count == 0);
}
int cisqFull(cqueue *q){
	return (q -> count == CMAX);
}
int noOfCharacters(cqueue *q){
	return q -> count;
}
void enqueueFromString(cqueue *q, unsigned char *str){
	int i = 0;
	while(str[i] != '\0'){
		cenqueue(q, str[i]);	
		i++;
	}
}
unsigned int getIntegerFromQueue(cqueue *q){
	unsigned int val = 0;
	int i;
	unsigned char ch;
	for(i = 0; i < 32; i++){
		if(!cisqEmpty(q)){
			ch = cdequeue(q);
			if(ch == '1'){
				val = val + pow(2, 31 - i);
			}
		}
	}
	return val;
}
unsigned int getIntFromQueue(cqueue *q){
	unsigned int val = 0;
	unsigned char ch;
	while(!cisqEmpty(q)){
		ch = cdequeue(q);
		if(ch == '1'){
			val = val + pow(2, q -> count);
		}
	}
	return val;
}
