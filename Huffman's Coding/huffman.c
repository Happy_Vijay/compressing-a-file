#include"htree.h"
#include<stdio.h>
#include<stdlib.h>
void leafOrder(node *h);
void huffman(unsigned long long *ascii){
	int i;
	unsigned long long f;
	htree h;
	node *p, *q, *r;
	init(&h);
	for(i = 0; i < 128; i++){
		if(ascii[i]){
			createAndInsertNode(&h, ascii[i], i + '\0'); //Take only non- zero frequency characters
		}	
	}
	while(1){
		if(!isTreeEmpty(&h)){
			p = removeNode(&h);
		}
		else{
			break;
		}
		if(!isTreeEmpty(&h)){
			q = removeNode(&h);
		}
		else{
			break;
		}
		r = (node*)malloc(sizeof(node));
		r -> left = p;
		r -> right = q;
		f = p -> freq + q -> freq;
		r -> freq = f;
		r -> ch = '\0';
		insertNode(&h, r);
	}
	leafOrder(p);
}
int ind = 0;
int code = 0;
char str[25];
void leafOrder(node *h){
	if(h){
		if(h -> left){
			str[ind++] = '0';
		}
		leafOrder(h -> left);
		if(h -> right){
			str[ind++] = '1';
		}
		leafOrder(h -> right);
		if(h -> left == NULL && h -> right == NULL){
			str[ind] = '\0';
			printf("%c %llu %s\n", h -> ch, h -> freq, str);
			code++;
		}
		ind --;
	}	
}
