#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<stdlib.h>
void huffman(unsigned long long *ascii);
int main(int argc, char *argv[]){
	int fd;
	char ch;
	unsigned long long ascii[128] = {0};
	fd = open(argv[1], O_RDONLY);
	if(fd == -1){
		printf("Open Failed");
		exit(1);
	}
	while(read(fd, &ch, 1)){
		ascii[ch - '\0']++;
	}
	huffman(ascii);
}
