#define MAX 256
/* Typedef's */
typedef struct node{		// Node will store the frequency of character and value of character
	unsigned long long freq;
	char ch;
	struct node *left, *right;		//By left, right pointers we can add an node with branches in the min-heap 
}node;
typedef struct htree{
	node *array[MAX];		// This array will help us in implementing the min heap(implicit-array index min heap)
	int n;				// To keep record of current position
}htree;
/* Function Prototypes */
void init(htree *h);			// Initializes the tree
node* createNode(unsigned long long freq, char ch);	//Creates a new Node
void insertNode(htree *h, node *m);		// Inserts a new Node in min-heap
node* removeNode(htree *h);			// Removes the node from tree and return it's address
unsigned long long getFreq(node *m);		// Returns frequency of character at a node
int smallest(node **a, int par, int c1, int c2);	
void createAndInsertNode(htree *h, unsigned long long freq, char ch);
int isTreeEmpty(htree *h);
int isTreeFull(htree *h);
